<?php

namespace Triangl\Tests;

use Triangl\WebTestCase;
use Triangl\BootstrapApplication;

/**
 * Functional test for Triangl Bootstrap.
 */
class BootstrapTest extends WebTestCase {
    /**
     * Implemented.
     */
    public function createApplication() {
        return new BootstrapApplication( 
            __DIR__ . "/../../../var", 
            array("test" => true) 
        );
    }
}
