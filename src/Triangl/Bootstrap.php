<?php

namespace Triangl;

use Silex\ServiceProviderInterface;

/*
 * Triangl bootstrap module.
 */
class Bootstrap implements ServiceProviderInterface {
    /**
     * Implemented.
     */
    public function boot(\Silex\Application $app) {
    }

    /**
     * Implemented.
     */
    public function register(\Silex\Application $app) {
        // Default configurations.
        $app["bootstrap.theme"] = null;
        $app["bootstrap.path"] = "https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0";
        
        // Register template folder.
        $app['twig.loader.filesystem']->prependPath(__DIR__ . "/../../views/Grid");
        $app['twig.loader.filesystem']->prependPath(__DIR__ . "/../../views/Form");
        $app['twig.loader.filesystem']->prependPath(__DIR__ . "/../../views/Navigation");
        
        // Register services.
        $app['triangl.bootstrap'] = $app->share( function ($app) {
            return new BootstrapHelper($app);
        } );
    }
}
