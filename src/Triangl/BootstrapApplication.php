<?php

namespace Triangl;

/**
 * Application with bootstrap module.
 */
class BootstrapApplication extends ContentManagementSystemApplication {
    /**
     * Overriden.
     */
    protected function init() {
        parent::init();
        
        $this->register( new Bootstrap() );
    }
}
