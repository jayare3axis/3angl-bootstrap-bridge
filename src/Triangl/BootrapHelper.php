<?php

namespace Triangl;

/**
 * Helper routines for backend module.
 */
class BootstrapHelper {
    private $app;
    
    /**
     * Default constructor.
     */
    public function __construct(Application $app) {
        $this->app = $app;
    }
    
    /**
     * Loads bootstrap library from assets folder.
     */
    public function loadBootstrap() {
        $this->app['assets']->addJavaScript( array(
            'path' => $this->app['jquery.path']
        ) );
        $this->app['assets']->addJavaScript( array(
            'path' => $this->app['bootstrap.path'] . '/js/bootstrap.min.js'
        ) );
        $this->app['assets']->addStyleSheet( array(
            'path' => $this->app['bootstrap.path'] . '/css/bootstrap.min.css'
        ) );
        $this->app['assets']->addJavaScript( array(
            'path' => $this->app['asset.path'] . '/js/ie-emulation-modes-warning.js'
        ) );
        $this->app['assets']->addJavaScript( array(
            'path' => $this->app['asset.path'] . '/js/ie10-viewport-bug-workaround.js'
        ) );        
        if ($this->app['bootstrap.theme']) {
            $this->app['assets']->addStyleSheet( array(
                'path' => $this->app['bootstrap.path'] . '/css/bootstrap.' . $this->app['bootstrap.theme'] . '.min.css'
            ) );
        }
    }
}
